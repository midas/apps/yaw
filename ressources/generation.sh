#!/bin/sh

css_url=$url/$css
edit_url=$edit_url


# Create a list of index.md paths ordered by folder structure
# format: create_pages_list
create_pages_list ()
{
  find . -type f -maxdepth 1 -name index.md | tree -dif -L 2 --noreport
}

# Create a list of all markdown paths
# format: create_md_files_list
create_md_files_list ()
{
  find . -type f -name "*.md"
}

# Create a markdown list from a folder structure, adding base URL
# format: create_md_list <path_list> <website_url>
create_md_list () 
{
  echo "* [Home]($2)"
  {
    read
    while read f ; do
      f=$(echo $f | sed s+\./++)
      echo $f | sed -e 's+[^/]*/+  +g' -e "s+[^ ]*$+* [&]($2/$f)+"
    done
  } < $1
}

# Generate an html <nav> element from a markdown file
# format: create_html_nav <markdown_file.md>
create_html_nav () 
{
    {
    echo "<button onclick=\"window.open('$edit_url'+window.location.pathname+'index.md','newwindow'); return false;\" id=\"edit_button\">EDIT</button>"
    echo '<nav id="mainNav"><nav id="mainNavContent">'
    cat $1
    echo '</nav></nav>'
    } | pandoc -f markdown -t html
}

# Convert all index.md to index.html
# format: pages_to_html <path_list> 
pages_to_html ()
{

  while read f ; do
  html_name=$(echo $f | sed s+\.md$+\.html+)
  pandoc "$f" -s --toc -o "$html_name" --include-before-body=menu.html --css=$css_url;
  rm "$f";
  done < $1
}

# Creation of a temporary copy
rm  -rf ~/wiki_temp/*
cp -r ~/wiki_md/* ~/wiki_temp
cd ~/wiki_temp

# Generation of the website in the copy
create_pages_list > list_temp 
create_md_list list_temp $url | create_html_nav > menu.html

create_md_files_list > md_list_temp
pages_to_html md_list_temp
rm list_temp md_list_temp
cp ~/ressources/themes/$css .
chmod -R a+r *

# Push to html folder
rm -rf ~/wiki_html/*
cp -r * ~/wiki_html
