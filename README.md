# Midas app: Yaw

A simple tool to generate a static wiki from markdown files

Yaw (for Yet Another Wiki) uses pandoc to convert a well organise bunch of markdown files to a static html wiki

This repository is part of the project **Midas** (for Minimalist Docker/Alpine Server). More infos in the [project wiki](https://gitlab.ensimag.fr/groups/midas/-/wikis/home).